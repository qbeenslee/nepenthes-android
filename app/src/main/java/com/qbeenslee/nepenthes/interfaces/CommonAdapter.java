package com.qbeenslee.nepenthes.interfaces;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * listview适用
 *
 * Created by qbeenslee on 2014/12/7.
 */

public abstract class CommonAdapter extends BaseAdapter {
    protected Context context;
    protected List<Map<String, Object>> mData = null;

    public CommonAdapter(Context context) {
        this.context = context;
    }

    public CommonAdapter(Context context, List<Map<String, Object>> data) {
        this.context = context;
        this.mData = data;
    }

    public void setData(List<Map<String, Object>> data) {
        this.mData = data;
        notifyDataSetChanged();
    }

    public void addData(List<Map<String, Object>> data) {
        if (mData != null) {
            mData.addAll(data);
            notifyDataSetChanged();
        } else {
            mData = new ArrayList<Map<String, Object>>();
            mData.addAll(data);
            notifyDataSetChanged();
        }
    }


    @Override
    public int getCount() {
        return mData != null ? mData.size() : 0;
    }

    @Override
    public Object getItem(int position) {
        return mData != null && position < mData.size() ? mData.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public abstract View getView(int position, View convertView, ViewGroup parent);

}

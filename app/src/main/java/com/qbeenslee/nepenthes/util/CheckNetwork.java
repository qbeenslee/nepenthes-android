package com.qbeenslee.nepenthes.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import com.qbeenslee.nepenthes.common.Message;

/**
 * 网络工具类
 * <p/>
 * Created by qbeenslee on 2014/12/8.
 */

public class CheckNetwork {

    //返回网络是否可用,需要权限
    public static boolean isAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isAvailable();
    }

    //判断网络连接状态
    public static int getNetType(Context context) {
        try {
            ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (connectivity != null) {
                NetworkInfo info = connectivity.getActiveNetworkInfo();
                if (info != null && info.isConnected()) {
                    if (info.getState() == NetworkInfo.State.CONNECTED) {
                        if (info.getType() == ConnectivityManager.TYPE_WIFI) {
                            return Message.STATUS.NET_TYPE_WIFI;
                        } else {
                            return Message.STATUS.NET_TYPE_MOBILE;
                        }
                    }
                }
            }
        } catch (Exception e) {
            return Message.STATUS.NET_ERROR;
        }
        return Message.STATUS.NET_ERROR;
    }

    //返回Wifi是否启用
    public static boolean isWIFIActivate(Context context) {
        return ((WifiManager) context.getSystemService(Context.WIFI_SERVICE))
                .isWifiEnabled();
    }

    //修改Wifi状态
    public static void changeWIFIStatus(Context context, boolean status) {
        ((WifiManager) context.getSystemService(Context.WIFI_SERVICE))
                .setWifiEnabled(status);
    }
}

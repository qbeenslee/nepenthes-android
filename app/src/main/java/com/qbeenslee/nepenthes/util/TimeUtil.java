package com.qbeenslee.nepenthes.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 时间工具
 * <p/>(待完善,添加友善的时间格式)
 * Created by qbeenslee on 2014/12/9.
 */

public class TimeUtil {
    public static final String FORMAT_DATE = "yyyy-MM-dd";
    public static final String FORMAT_TIME = "HH:mm:ss";
    public static final String FORMAT_CHINESE_DATETIME = "yyyy年MM月dd日 HH:mm:ss";
    public static final String FORMAT_DATETIME_SHORT = "yyyyMMddHHmmss";
    public static final String FORMAT_DATETIME_DEFAULT = "yyyy-MM-dd HH:mm:ss";
    private static final SimpleDateFormat sdf = new SimpleDateFormat();
    private static final int YEAR = 365 * 24 * 60 * 60;
    private static final int MONTH = 30 * 24 * 60 * 60;
    private static final int DAY = 24 * 60 * 60;
    private static final int HOUR = 60 * 60;
    private static final int MINUTE = 60;

    public static class Now {
        private String format;
        private Date nowDate;
        private long now;

        public Now() {
            this.nowDate = new Date();
            this.now = System.currentTimeMillis();
        }

        public Now setFormat(String format) {
            this.format = format;
            return this;
        }

        public String getShortTimeString() {
            return FormatTime(nowDate, FORMAT_DATETIME_SHORT);
        }

        public String getTimeString() {
            return FormatTime(nowDate, FORMAT_DATETIME_DEFAULT);
        }

        public String getChineseFormatTimeString() {
            return FormatTime(nowDate, FORMAT_CHINESE_DATETIME);
        }

        public String getTimeStirngByFormat(String format) {
            return FormatTime(nowDate, format);
        }
    }

    public static String FormatTime(Date time, String format) {
        try {
            sdf.applyPattern(format);
        } catch (IllegalArgumentException e) {
            sdf.applyPattern(FORMAT_DATETIME_DEFAULT);
        } catch (Exception e) {
            return "";
        }
        return sdf.format(time);
    }
}

package com.qbeenslee.nepenthes.util;

import android.util.Log;

import com.qbeenslee.nepenthes.BuildConfig;

/**
 * Log工具类，设置开关，防止发布版本时log信息泄露
 *
 * Created by qbeenslee on 2014/12/8.
 */

public class DebugLog {
    public static void v(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.v(tag, msg);
        }
    }

    public static void d(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg);
        }
    }

    public static void i(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.i(tag, msg);
        }
    }

    public static void w(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.w(tag, msg);
        }
    }

    public static void e(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.e(tag, msg);
        }
    }
}

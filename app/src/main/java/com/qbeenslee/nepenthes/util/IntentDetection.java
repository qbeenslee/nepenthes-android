package com.qbeenslee.nepenthes.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;

import java.util.List;

/**
 * Intent检测
 * Created by qbeenslee on 2014/12/7.
 */

public class IntentDetection {

    //检测Intent是否可以被响应
    public static boolean isAvailableIntent(Context context, String action) {
        final PackageManager pm = context.getPackageManager();
        final Intent intent = new Intent(action);
        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return resolveInfos.size() > 0;
    }


    public static boolean isAvailableIntent(Context context, Intent intent) {
        final PackageManager pm = context.getPackageManager();
        List<ResolveInfo> resolveInfos = pm.queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return resolveInfos.size() > 0;
    }
}

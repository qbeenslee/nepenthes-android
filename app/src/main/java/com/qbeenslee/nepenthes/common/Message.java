package com.qbeenslee.nepenthes.common;

/**
 * 通信标准接口
 * Created by qbeenslee on 2014/12/2.
 */

public interface Message {

    public interface HANDLER {
        public static final int LEFTMENU_USER = 0x1001;
        public static final int LEFTMENU_INFORMATION = 0x1002;
        public static final int LEFTMENU_SETTING = 0x1003;
        public static final int LEFTMENU_EXIT = 0x1004;
        public static final int LEFTMENU_ABOUT = 0x1005;
        public static final int DIALOG_TAKEPHOTO = 0x1006;
        public static final int DIALOG_CHOOSEGALLERY = 0x1007;
    }

    public interface SIGN {
        public static final String TAG = "TAG";
        public static final String FRAGMENT_MANAGER = "FRAGMENTMANAGER";
    }

    public interface NAVI {
        public static final int PHOTOWALL = 0;
        public static final int FAVORITE = 1;
    }

    public interface CODE {
        public static final int REQUEST_CHOOSEGALLERY = 0x1001;
        public static final int REQUEST_TAKEPHOTO = 0x1001;
    }

    public interface STATUS {
        public static final int NET_ERROR = 0;
        public static final int NET_TYPE_WIFI = 1;
        public static final int NET_TYPE_MOBILE = 2;
    }


    public interface SQL {

    }
}

package com.qbeenslee.nepenthes.annotation;

/**
 *#
 * Created by qbeenslee on 2014/11/23.
 */

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD})
public @interface Column {
    public abstract String value();
}

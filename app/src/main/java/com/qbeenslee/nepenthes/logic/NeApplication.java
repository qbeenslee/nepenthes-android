package com.qbeenslee.nepenthes.logic;

import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.DisplayMetrics;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import com.qbeenslee.nepenthes.util.DebugLog;

/**
 * Application扩展 (待添加用户实体类)
 * Created by qbeenslee on 2014/11/27.
 */

public class NeApplication extends Application {
    private static final String TAG = "NeApplication";

    public static final String DB = "zfjc.sqlite";

    public static final String APKNAME = "zfjc.apk";

    public static final String PHOTO = "photo";

    public static final String WFXM_PHOTO = "wfxm";

    public static final String XGD_PHOTO = "xgd";

    private static NeApplication app = null;

    public static String BASEPATH = null, DBPATH = null, APKPATH = null;

    public static String photoPath, wfxmPhotoPath, xgdPhotoPath;

    public static int srid = 2364;

    public static float density = 1.0f;

    public static int xvalue = 0;

    public static int yvalue = 0;

    private AppConfig appConfig = null;

    private ActivityManager activityManager = null;

    private TaskService mService;

    private ServiceConnection mConnection = null;

    private boolean isBind = false;


    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        activityManager = ActivityManager.getInstance();
        initApp();
        startService();
    }

    @Override
    public void onTerminate() {
        DebugLog.d(TAG, "onTerminate");
        super.onTerminate();
    }

    private void initApp() {
        ImageLoaderConfiguration configuration = ImageLoaderConfiguration.createDefault(this);
        ImageLoader.getInstance().init(configuration);

        // 获取系统配置
        appConfig = new AppConfig(getApplicationContext());
        // 设置路径
        BASEPATH = appConfig.getFilePath() + "/";
        DBPATH = BASEPATH + DB;
        APKPATH = BASEPATH + APKNAME;
        photoPath = BASEPATH + PHOTO + "/";
        wfxmPhotoPath = photoPath + WFXM_PHOTO;
        xgdPhotoPath = photoPath + XGD_PHOTO;
        // 获取屏幕密度
        DisplayMetrics metrics = getApplicationContext().getResources()
                .getDisplayMetrics();
        density = metrics.density;
        xvalue = metrics.widthPixels;
        yvalue = metrics.heightPixels;

        DebugLog.d(TAG, "density[" + density + "];xvalue[" + xvalue + "];yvalue["
                + yvalue + "]");
        // 新建服务链接
        mConnection = new ServiceConnection() {
            @Override
            public void onServiceDisconnected(ComponentName name) {
                mService = null;
                isBind = false;
            }

            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                mService = ((TaskService.AppBinder) service).getService();
                isBind = true;
            }
        };
        // 读取界面配置
        // UIConfigParser parser = new UIConfigParser(getApplicationContext());
        // configs = parser.parse();
    }

    private void startService() {
        Intent intent = new Intent(getApplicationContext(), TaskService.class);
        startService(intent);
        bindService(intent, mConnection, Context.BIND_AUTO_CREATE);
    }

    private void stopService() {
        unbindService(mConnection);
        Intent intent = new Intent(getApplicationContext(), TaskService.class);
        stopService(intent);
    }


    public void updatePathConfig() {
        BASEPATH = appConfig.getFilePath() + "/";
        DBPATH = BASEPATH + DB;
        DebugLog.d(TAG, "dbPath[" + DBPATH + "]");
    }

    public void exitApp() {
        stopService();
        activityManager.popAllActivity();
        System.exit(0);
    }

    public ActivityManager getActivityManager() {
        return activityManager;
    }

    public static NeApplication getApp() {
        return app;
    }

    public AppConfig getAppConfig() {
        return appConfig;
    }

    public TaskService getmService() {
        return mService;
    }

    /*public BtUser getUser() {
        return user;
    }

    public void setUser(BtUser _user) {
        this.user = _user;
    }*/


}

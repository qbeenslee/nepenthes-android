package com.qbeenslee.nepenthes.logic;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Camera;

/**
 * 应用设定
 * Created by qbeenslee on 2014/11/27.
 */

public class AppConfig {
    private static final String CONFIG_NAME = "appConfig";

    private static final String DEFAULT_IP = "192.168.1.137";

    private static final String DEFAULT_URL = "";

    private static final String DEFAULT_FILEPATH = "";

    private static final String DEFAULT_USERNANE = "admin";

    private static final String DEFAULT_PASSWORD = "1";

    private static final Boolean DEFAULT_ISREMEMBERPWD = false;

    private String host = "host";

    private String url = "url";

    private String filePath = "filePath";

    private String userName = "userName";

    private String passWord = "passWord";

    private String isRememberPwd = "isRememberPwd";

    private static final String FLASHMODE = "flashmode";

    private SharedPreferences sharedPreferences;

    private SharedPreferences.Editor editor;

    public AppConfig(Context context) {
        this(context, CONFIG_NAME);
    }

    public AppConfig(Context context, String configName) {
        sharedPreferences = context.getSharedPreferences(configName,
                Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }


    public void putSharePreStr(String key, String value) {
        editor.putString(key, value).apply();
    }

    public void putSharePreInt(String key, int value) {
        editor.putInt(key, value).apply();
    }

    public void putSharePreBoolean(String key, boolean value) {
        editor.putBoolean(key, value).apply();
    }

    public void putSharePreLong(String key, long value) {
        editor.putLong(key, value).apply();
    }

    public void putSharePreFloat(String key, float value) {
        editor.putFloat(key, value).apply();
    }

    public String getSharePreStr(String key, String defValue) {
        return sharedPreferences.getString(key, defValue);
    }

    public int getSharePreInt(String key, int defValue) {
        return sharedPreferences.getInt(key, defValue);
    }

    public boolean getSharePreBoolean(String key, boolean defValue) {
        return sharedPreferences.getBoolean(key, defValue);
    }

    public long getSharePreLong(String key, long defValue) {
        return sharedPreferences.getLong(key, defValue);
    }

    public float getSharePreFloat(String key, float defValue) {
        return sharedPreferences.getFloat(key, defValue);
    }

    public String getWebserviceUrl() {
        return "http://" + getHost() + "/" + getUrl() + "/";
    }

    public String getTestWebserviceUrl() {
        return "http://" + getHost() + "/" + getUrl();
    }

    public String getPassWord() {
        return getSharePreStr(this.passWord, DEFAULT_PASSWORD);
    }

    public void setPassWord(String passWord) {
        putSharePreStr(this.passWord, passWord);
    }

    public String getUserName() {
        return getSharePreStr(this.userName, DEFAULT_USERNANE);
    }

    public void setUserName(String userName) {
        putSharePreStr(this.userName, userName);
    }

    public boolean getIsRememberPwd() {
        return getSharePreBoolean(this.isRememberPwd, DEFAULT_ISREMEMBERPWD);
    }

    public void setIsRememberPwd(boolean isRememberPwd) {
        putSharePreBoolean(this.isRememberPwd, isRememberPwd);
    }

    public String getFilePath() {
        return getSharePreStr(this.filePath, DEFAULT_FILEPATH);
    }

    public void setFilePath(String filePath) {
        putSharePreStr(this.filePath, filePath);
    }

    public String getFlashMode() {
        return sharedPreferences.getString(FLASHMODE,
                Camera.Parameters.FLASH_MODE_OFF);
    }

    public void setFlashMode(String flashMode) {
        editor.putString(FLASHMODE, flashMode).apply();
    }

    public String getHost() {
        return sharedPreferences.getString(this.host, DEFAULT_IP);
    }

    public void setHost(String host) {
        editor.putString(this.host, host).apply();
    }

    public String getUrl() {
        return sharedPreferences.getString(this.url, DEFAULT_URL);
    }

    public void setUrl(String url) {
        editor.putString(this.url, url).apply();
    }

}

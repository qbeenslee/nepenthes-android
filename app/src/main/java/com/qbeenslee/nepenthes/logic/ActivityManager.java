package com.qbeenslee.nepenthes.logic;

import android.app.Activity;

import java.util.Stack;

/**
 * # Manage Activity backstack
 * Created by qbeenslee on 2014/11/27.
 */

public class ActivityManager {
    private static Stack<Activity> activityStack;

    private static ActivityManager instant;

    private ActivityManager() {
        activityStack = new Stack<Activity>();
    }

    public static synchronized ActivityManager getInstance() {
        if (instant == null) {
            instant = new ActivityManager();
        }
        return instant;
    }

    public void popActivity(Activity activity) {
        if (activity != null) {
            activity.finish();
            activityStack.remove(activity);
            activity = null;
        }
    }

    public Activity currentActivity() {
        Activity activity = null;
        if (!activityStack.empty()) {
            activity = activityStack.lastElement();
        }
        return activity;
    }

    public void pushActivity(Activity activity) {
        activityStack.add(activity);
    }

    public void popAllActivityExceptOne(Class<?> cls) {
        while (true) {
            Activity activity = currentActivity();
            if (activity == null) {
                break;
            }
            if (activity.getClass().equals(cls)) {
                break;
            }
            popActivity(activity);
        }
    }

    public void popAllActivity() {
        while (true) {
            Activity activity = currentActivity();
            if (activity == null) {
                break;
            }
            popActivity(activity);
        }
    }

    public Activity queryActivity(Class<?> cls) {
        Activity activity = null;

        if (activityStack != null && !activityStack.empty()) {
            synchronized (activityStack) {
                for (Activity tmpActivity : activityStack) {
                    if (tmpActivity.getClass().equals(cls)) {
                        activity = tmpActivity;
                        break;
                    }
                }
            }
        }
        return activity;
    }

    public void finishActivity(Activity activity) {
        if (activity != null) {
            activityStack.remove(activity);
            activity.finish();
            activity = null;
        }
    }

}

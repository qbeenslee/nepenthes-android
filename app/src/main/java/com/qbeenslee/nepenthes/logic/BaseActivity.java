package com.qbeenslee.nepenthes.logic;

import android.app.AlertDialog;
import android.content.DialogInterface.OnClickListener;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.qbeenslee.nepenthes.R;

/**
 * #管理
 * Created by qbeenslee on 2014/11/27.
 */

public class BaseActivity extends FragmentActivity {

    protected NeApplication app;

    protected AlertDialog dialog = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (NeApplication) getApplication();
        app.getActivityManager().pushActivity(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        app.getActivityManager().popActivity(this);
    }

    public void alertDialog(CharSequence title, CharSequence message) {
        String btnOneText = getResources().getString(R.string.ok);
        alertDialog(title, message, btnOneText, null, null, null, null, null);
    }

    public void alertDialog(int titleId, int messageId) {
        alertDialog(titleId, messageId, R.string.ok, null, 0, null, 0, null);
    }

    public void alertDialog(int titleId, int messageId, int btnOneTextId, OnClickListener btnOneListener) {
        alertDialog(titleId, messageId, btnOneTextId, btnOneListener, 0, null, 0, null);
    }

    public void alertDialog(int titleId, int messageId, int btnOneTextId, OnClickListener btnOneListener, int btnTwoTextId, OnClickListener btnTwoListener) {
        alertDialog(titleId, messageId, btnOneTextId, btnOneListener, btnTwoTextId, btnTwoListener, 0, null);
    }

    public void alertDialog(CharSequence title, CharSequence message, CharSequence btnOneText, OnClickListener btnOneListener, CharSequence btnTwoText, OnClickListener btnTwoListener) {
        alertDialog(title, message, btnOneText, btnOneListener, btnTwoText, btnTwoListener, null, null);
    }

    public void alertDialog(CharSequence title, CharSequence message, CharSequence btnOneText, OnClickListener btnOneListener, CharSequence btnTwoText, OnClickListener btnTwoListener, CharSequence btnThreeText, OnClickListener btnThreeListener) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(message).setCancelable(false);
        if (btnOneText != null) {
            builder.setPositiveButton(btnOneText, btnOneListener);
        }
        if (btnTwoText != null) {
            builder.setNegativeButton(btnTwoText, btnTwoListener);
        }
        if (btnThreeText != null) {
            builder.setNeutralButton(btnThreeText, btnThreeListener);
        }
        dialog = builder.create();
        dialog.show();
    }

    public void alertDialog(int titleId, int messageId, int btnOneTextId, OnClickListener btnOneListener, int btnTwoTextId, OnClickListener btnTwoListener, int btnThreeTextId, OnClickListener btnThreeListener) {
        String title = null, message = null, btnOneText = null, btnTwoText = null, btnThreeText = null;
        title = getResources().getString(titleId);
        message = getResources().getString(messageId);
        if (btnOneTextId != 0) {
            btnOneText = getResources().getString(btnOneTextId);
        }
        if (btnTwoTextId != 0) {
            btnTwoText = getResources().getString(btnTwoTextId);
        }
        if (btnThreeTextId != 0) {
            btnThreeText = getResources().getString(btnThreeTextId);
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title).setMessage(message).setCancelable(false);
        if (btnOneText != null) {
            builder.setPositiveButton(btnOneText, btnOneListener);
        }
        if (btnTwoText != null) {
            builder.setNegativeButton(btnTwoText, btnTwoListener);
        }
        if (btnThreeText != null) {
            builder.setNeutralButton(btnThreeText, btnThreeListener);
        }
        dialog = builder.create();
        dialog.show();
    }

    public void dismissDialog() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
    }

}

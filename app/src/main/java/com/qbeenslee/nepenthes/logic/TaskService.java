package com.qbeenslee.nepenthes.logic;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;

/**
 * 后台任务
 * Created by qbeenslee on 2014/11/27.
 */

public class TaskService extends Service {
    private static final String TAG = "BACKSERVICE";
    private static EventHandler handler;
    private Looper mLooper;
    private HandlerThread thread;

    @Override
    public void onCreate() {
        //System.out.println("IN");
        super.onCreate();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public TaskService() {
        super();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new AppBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    public class AppBinder extends Binder {
        public TaskService getService() {
            return TaskService.this;
        }
    }
}

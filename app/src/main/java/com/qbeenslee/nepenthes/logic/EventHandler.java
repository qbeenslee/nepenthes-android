package com.qbeenslee.nepenthes.logic;

/**
 * #
 * Created by qbeenslee on 2014/11/27.
 */

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import java.util.HashMap;


public class EventHandler extends Handler {
    private Context mContext;

    private UIHandler uiHandler;

    public EventHandler(Looper looper, Context context, UIHandler handler) {
        super(looper);
        this.mContext = context;
        this.uiHandler = handler;
    }

    @Override
    @SuppressWarnings("unchecked")
    public void handleMessage(Message msg) {
        HashMap<String, Object> map;
        switch (msg.what) {
        /*case SocketAction.SOCKET_RECEIVE:
            if (msg.obj != null) {
				try {
					String content = new String((byte[]) msg.obj, "UTF-8");
					System.out.println("收到消息："+content);
					int cmd = Integer.valueOf(content.substring(0, 2));
					String data = content.substring(2);
					switch (cmd) {
					case SocketCmd.CMD_LOGIN:
						uiHandler.obtainMessage(123, data).sendToTarget();
						break;
					case SocketCmd.CMD_ALIVE:
						break;
					case SocketCmd.CMD_ANALYSEDKRESULT:
						String []str=(content.substring(2,content.length())).split("\\|");
						String []mjs=str[1].split("@");
						Database db=new Database(ApplicationEx.ZFJCDBPATH);
						if(db.openExistingDatabase())
						{
							String sql="update T_WFXM set JSYD_MJ='"+mjs[0]+"' ,WLYD_MJ='"+mjs[1]+"' ,NYD_MJ='"+mjs[2]+"' ,GD_MJ='"+mjs[3]+"' ,JBNT_MJ='"+mjs[4]+"' ,FHGH_MJ='"+mjs[5]+"',BFHGH_MJ='"+mjs[6]+"'   where WFXM_GUID='"+str[0]+"' ";
							db.executeNonQuery(sql);
							db.closeDatabase();
						}
						break;
					}

				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			}
			break;*/
            default:
                uiHandler.obtainMessage(msg.what, msg.arg1, msg.arg2, msg.obj)
                        .sendToTarget();
                break;
        }
        super.handleMessage(msg);
    }
}

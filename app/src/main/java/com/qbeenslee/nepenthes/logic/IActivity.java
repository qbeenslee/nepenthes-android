package com.qbeenslee.nepenthes.logic;

/**
 * #
 * Created by qbeenslee on 2014/11/27.
 */

public interface IActivity {
    void init();

    void refresh(Object... param);
}

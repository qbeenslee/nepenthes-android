package com.qbeenslee.nepenthes.handler;


import android.os.Handler;
import android.os.Message;

/**
 * 主界面Handler处理
 * Created by qbeenslee on 2014/11/23.
 */

public class HomeHandler extends Handler {
    @Override
    public void dispatchMessage(Message msg) {
        super.dispatchMessage(msg);
    }

    public HomeHandler(Callback callback) {
        super(callback);
    }
}

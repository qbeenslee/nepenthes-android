package com.qbeenslee.nepenthes.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.qbeenslee.nepenthes.R;

/**
 * Favorite
 * Created by qbeenslee on 2014/12/2.
 */
public class FavoriteFragment extends Fragment {
    public static final String TAG = "FavoriteFragment";
    private View view = null;


    public FavoriteFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_favorite, container, false);
        return view;
    }


}

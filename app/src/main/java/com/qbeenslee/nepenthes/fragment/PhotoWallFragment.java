package com.qbeenslee.nepenthes.fragment;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.qbeenslee.nepenthes.R;
import com.qbeenslee.nepenthes.adapter.PhotoWallAdapter;


/**
 * PhotoWall
 * Created by qbeenslee on 2014/12/2.
 */
public class PhotoWallFragment extends Fragment implements OnRefreshListener {
    public static final String TAG = "PhotoWallFragment";
    private View view;
    private PullToRefreshListView lvPhtowall = null;
    private PhotoWallAdapter pwAdapter = null;
    private Context context = null;

    public PhotoWallFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_photowall, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pwAdapter = new PhotoWallAdapter(context);
        pwAdapter.setData(PhotoWallAdapter.testData());
        lvPhtowall = (PullToRefreshListView) view.findViewById(R.id.listview_phtowall);
        lvPhtowall.setAdapter(pwAdapter);
        lvPhtowall.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh(PullToRefreshBase pullToRefreshBase) {
        new GetDataTask().execute();
    }

    private class GetDataTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            // Simulates a background job.
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
            }
            String str = "Added after refresh...I add";
            return str;
        }

        @Override
        protected void onPostExecute(String result) {
            pwAdapter.addData(PhotoWallAdapter.testData());
            lvPhtowall.onRefreshComplete();
            super.onPostExecute(result);
        }
    }
}

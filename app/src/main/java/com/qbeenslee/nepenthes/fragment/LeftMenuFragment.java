package com.qbeenslee.nepenthes.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;

import com.qbeenslee.nepenthes.R;
import com.qbeenslee.nepenthes.common.Message;
import com.qbeenslee.nepenthes.ui.HomeActivity;

/**
 * #
 * Created by qbeenslee on 2014/11/30.
 */

public class LeftMenuFragment extends Fragment implements OnClickListener {
    public static final String TAG = "LeftMenuFragment";
    private View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_leftmenu, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        getAndSetViews();
        super.onViewCreated(view, savedInstanceState);
    }

    public void getAndSetViews() {
        if (view != null) {
            view.findViewById(R.id.leftmenu_about).setOnClickListener(this);
            view.findViewById(R.id.leftmenu_exit).setOnClickListener(this);
            view.findViewById(R.id.leftmenu_information).setOnClickListener(this);
            view.findViewById(R.id.leftmenu_setting).setOnClickListener(this);
            view.findViewById(R.id.leftmenu_user_head).setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.leftmenu_about: {
                HomeActivity.sendMessage(Message.HANDLER.LEFTMENU_ABOUT);
            }
            break;
            case R.id.leftmenu_exit: {
                HomeActivity.sendMessage(Message.HANDLER.LEFTMENU_EXIT);
            }
            break;
            case R.id.leftmenu_information: {
                HomeActivity.sendMessage(Message.HANDLER.LEFTMENU_INFORMATION);
            }
            break;
            case R.id.leftmenu_setting: {
                HomeActivity.sendMessage(Message.HANDLER.LEFTMENU_SETTING);
            }
            break;
            case R.id.leftmenu_user_head: {
                HomeActivity.sendMessage(Message.HANDLER.LEFTMENU_USER);
            }
            break;
        }
    }
}

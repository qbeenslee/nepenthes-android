package com.qbeenslee.nepenthes.entity;

import com.qbeenslee.nepenthes.annotation.Column;

/**
 * #
 * Created by qbeenslee on 2014/12/9.
 */

public class User {
    @Column("uid")
    private String id;
    @Column("username")
    private String name;
    @Column("imei")
    private String imei;
    @Column("motto")
    private String motto;
    @Column("verify_status")
    private String verifyStatus;
    @Column("level")
    private String level;
    @Column("is_active")
    private String isActive;

}

package com.qbeenslee.nepenthes.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.qbeenslee.nepenthes.R;
import com.qbeenslee.nepenthes.interfaces.CommonAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * #
 * Created by qbeenslee on 2014/12/7.
 */

public class PhotoWallAdapter extends CommonAdapter {
    public PhotoWallAdapter(Context context) {
        super(context);
    }

    public PhotoWallAdapter(Context context, List<Map<String, Object>> data) {
        super(context, data);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater li = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = li.inflate(R.layout.item_photowall, parent, false);
            convertView.setFocusable(false);
        }
        if (mData != null) {
            Map<String, Object> one = mData.get(position);
            TextView tv = (TextView) convertView.findViewById(R.id.textview_t);
            tv.setText(one.get("T").toString());
        }
        return convertView;
    }

    @Deprecated
    public static List<Map<String, Object>> testData() {
        String[] mStrings = {"Abbaye de Belloc", "Abbaye du Mont des Cats", "Abertam", "Abondance", "Ackawi",
                "Acorn", "Adelost", "Affidelice au Chablis", "Afuega'l Pitu", "Airag", "Airedale", "Aisy Cendre",
                "Allgauer Emmentaler", "Abbaye de Belloc", "Abbaye du Mont des Cats", "Abertam", "Abondance", "Ackawi",
                "Acorn", "Adelost", "Affidelice au Chablis", "Afuega'l Pitu", "Airag", "Airedale", "Aisy Cendre",
                "Allgauer Emmentaler"};
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>(100);
        for (String item : mStrings) {
            Map<String, Object> map = new HashMap<String, Object>(1);
            map.put("T", item);
            list.add(map);
        }
        return list;
    }
}

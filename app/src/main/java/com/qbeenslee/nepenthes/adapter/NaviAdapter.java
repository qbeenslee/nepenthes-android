package com.qbeenslee.nepenthes.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.qbeenslee.nepenthes.fragment.FavoriteFragment;
import com.qbeenslee.nepenthes.fragment.PhotoWallFragment;

import java.util.ArrayList;

/**
 * #
 * Created by qbeenslee on 2014/12/3.
 */

public class NaviAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> mFragments;

    public NaviAdapter(FragmentManager fm) {
        super(fm);
        mFragments = new ArrayList<Fragment>();
        mFragments.add(new PhotoWallFragment());
        mFragments.add(new FavoriteFragment());
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position);
    }

}

package com.qbeenslee.nepenthes.widget;

/**
 * 环形图像
 * Created by qbeenslee on 2014/11/30.
 */

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.qbeenslee.nepenthes.R;

public class CircleImageView extends ImageView {

    public static final String TAG = "CircleImageView";

    public static final int DEFAULT_RADIUS = 0;
    public static final int DEFAULT_BORDER = 0;

    private int mCornerRadius;
    private int mBorderWidth;
    private ColorStateList mBorderColor;

    private boolean mRoundBackground = false;
    private boolean mOval = false;

    private Drawable mDrawable;
    private Drawable mBackgroundDrawable;

    private ScaleType mScaleType;

    private static final ScaleType[] sScaleTypeArray = {
            ScaleType.MATRIX,
            ScaleType.FIT_XY,
            ScaleType.FIT_START,
            ScaleType.FIT_CENTER,
            ScaleType.FIT_END,
            ScaleType.CENTER,
            ScaleType.CENTER_CROP,
            ScaleType.CENTER_INSIDE
    };

    public CircleImageView(Context context) {
        super(context);
        mCornerRadius = DEFAULT_RADIUS;
        mBorderWidth = DEFAULT_BORDER;
        mBorderColor = ColorStateList.valueOf(CircleDrawable.DEFAULT_BORDER_COLOR);
    }

    public CircleImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.CircleImageView, defStyle, 0);

        int index = a.getInt(R.styleable.CircleImageView_android_scaleType, -1);
        if (index >= 0) {
            setScaleType(sScaleTypeArray[index]);
        }

        mCornerRadius = a.getDimensionPixelSize(R.styleable.CircleImageView_corner_radius, -1);
        mBorderWidth = a.getDimensionPixelSize(R.styleable.CircleImageView_border_width, -1);

        // don't allow negative values for radius and border
        if (mCornerRadius < 0) {
            mCornerRadius = DEFAULT_RADIUS;
        }
        if (mBorderWidth < 0) {
            mBorderWidth = DEFAULT_BORDER;
        }

        mBorderColor = a.getColorStateList(R.styleable.CircleImageView_border_color);
        if (mBorderColor == null) {
            mBorderColor = ColorStateList.valueOf(CircleDrawable.DEFAULT_BORDER_COLOR);
        }

        mRoundBackground = a.getBoolean(R.styleable.CircleImageView_cirlce_background, false);
        mOval = a.getBoolean(R.styleable.CircleImageView_is_oval, false);

        if (mDrawable instanceof CircleDrawable) {
            updateDrawableAttrs((CircleDrawable) mDrawable);
        }

        if (mRoundBackground) {
            if (!(mBackgroundDrawable instanceof CircleDrawable)) {
                // try setting background drawable now that we got the mRoundBackground param
                setBackgroundDrawable(mBackgroundDrawable);
            }
            if (mBackgroundDrawable instanceof CircleDrawable) {
                updateDrawableAttrs((CircleDrawable) mBackgroundDrawable);
            }
        }

        a.recycle();
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        invalidate();
    }

    /**
     * Controls how the image should be resized or moved to match the size
     * of this ImageView.
     *
     * @param scaleType The desired scaling mode.
     * @attr ref android.R.styleable#ImageView_scaleType
     */
    @Override
    public void setScaleType(ScaleType scaleType) {
        if (scaleType == null) {
            throw new NullPointerException();
        }

        if (mScaleType != scaleType) {
            mScaleType = scaleType;

            switch (scaleType) {
                case CENTER:
                case CENTER_CROP:
                case CENTER_INSIDE:
                case FIT_CENTER:
                case FIT_START:
                case FIT_END:
                case FIT_XY:
                    super.setScaleType(ScaleType.FIT_XY);
                    break;
                default:
                    super.setScaleType(scaleType);
                    break;
            }

            if (mDrawable instanceof CircleDrawable
                    && ((CircleDrawable) mDrawable).getScaleType() != scaleType) {
                ((CircleDrawable) mDrawable).setScaleType(scaleType);
            }

            if (mBackgroundDrawable instanceof CircleDrawable
                    && ((CircleDrawable) mBackgroundDrawable).getScaleType() != scaleType) {
                ((CircleDrawable) mBackgroundDrawable).setScaleType(scaleType);
            }
            setWillNotCacheDrawing(true);
            requestLayout();
            invalidate();
        }
    }

    /**
     * Return the current scale type in use by this ImageView.
     *
     * @attr ref android.R.styleable#ImageView_scaleType
     * @see android.widget.ImageView.ScaleType
     */
    @Override
    public ScaleType getScaleType() {
        return mScaleType;
    }


    @Override
    public void setImageDrawable(Drawable drawable) {
        if (drawable != null) {
            mDrawable = CircleDrawable.fromDrawable(drawable, mCornerRadius, mBorderWidth, mBorderColor, mOval);
            updateDrawableAttrs((CircleDrawable) mDrawable);
        } else {
            mDrawable = null;
        }
        super.setImageDrawable(mDrawable);
    }

    public void setImageBitmap(Bitmap bm) {
        if (bm != null) {
            mDrawable = new CircleDrawable(bm, mCornerRadius, mBorderWidth, mBorderColor, mOval);
            updateDrawableAttrs((CircleDrawable) mDrawable);
        } else {
            mDrawable = null;
        }
        super.setImageDrawable(mDrawable);
    }

    @Override
    public void setBackground(Drawable background) {
        setBackgroundDrawable(background);
    }

    private void updateDrawableAttrs(CircleDrawable drawable) {
        drawable.setScaleType(mScaleType);
        drawable.setCornerRadius(mCornerRadius);
        drawable.setBorderWidth(mBorderWidth);
        drawable.setBorderColors(mBorderColor);
        drawable.setOval(mOval);
    }

    @Override
    @Deprecated
    public void setBackgroundDrawable(Drawable background) {
        if (mRoundBackground && background != null) {
            mBackgroundDrawable = CircleDrawable.fromDrawable(background, mCornerRadius, mBorderWidth, mBorderColor, mOval);
            updateDrawableAttrs((CircleDrawable) mBackgroundDrawable);
        } else {
            mBackgroundDrawable = background;
        }
        super.setBackgroundDrawable(mBackgroundDrawable);
    }

    public int getCornerRadius() {
        return mCornerRadius;
    }

    public int getBorder() {
        return mBorderWidth;
    }

    public int getBorderColor() {
        return mBorderColor.getDefaultColor();
    }

    public ColorStateList getBorderColors() {
        return mBorderColor;
    }

    public void setCornerRadius(int radius) {
        if (mCornerRadius == radius) {
            return;
        }

        mCornerRadius = radius;
        if (mDrawable instanceof CircleDrawable) {
            ((CircleDrawable) mDrawable).setCornerRadius(radius);
        }
        if (mRoundBackground && mBackgroundDrawable instanceof CircleDrawable) {
            ((CircleDrawable) mBackgroundDrawable).setCornerRadius(radius);
        }
    }

    public void setBorderWidth(int width) {
        if (mBorderWidth == width) {
            return;
        }

        mBorderWidth = width;
        if (mDrawable instanceof CircleDrawable) {
            ((CircleDrawable) mDrawable).setBorderWidth(width);
        }
        if (mRoundBackground && mBackgroundDrawable instanceof CircleDrawable) {
            ((CircleDrawable) mBackgroundDrawable).setBorderWidth(width);
        }
        invalidate();
    }

    public void setBorderColor(int color) {
        setBorderColors(ColorStateList.valueOf(color));
    }

    public void setBorderColors(ColorStateList colors) {
        if (mBorderColor.equals(colors)) {
            return;
        }

        mBorderColor = colors != null ? colors : ColorStateList.valueOf(CircleDrawable.DEFAULT_BORDER_COLOR);
        if (mDrawable instanceof CircleDrawable) {
            ((CircleDrawable) mDrawable).setBorderColors(colors);
        }
        if (mRoundBackground && mBackgroundDrawable instanceof CircleDrawable) {
            ((CircleDrawable) mBackgroundDrawable).setBorderColors(colors);
        }
        if (mBorderWidth > 0) {
            invalidate();
        }
    }

    public void setOval(boolean oval) {
        mOval = oval;
        if (mDrawable instanceof CircleDrawable) {
            ((CircleDrawable) mDrawable).setOval(oval);
        }
        if (mRoundBackground && mBackgroundDrawable instanceof CircleDrawable) {
            ((CircleDrawable) mBackgroundDrawable).setOval(oval);
        }
        invalidate();
    }

    public boolean isOval() {
        return mOval;
    }

    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        setImageDrawable(getDrawable());
    }

    public boolean isRoundBackground() {
        return mRoundBackground;
    }

    public void setRoundBackground(boolean roundBackground) {
        if (mRoundBackground == roundBackground) {
            return;
        }

        mRoundBackground = roundBackground;
        if (roundBackground) {
            if (mBackgroundDrawable instanceof CircleDrawable) {
                updateDrawableAttrs((CircleDrawable) mBackgroundDrawable);
            } else {
                setBackgroundDrawable(mBackgroundDrawable);
            }
        } else if (mBackgroundDrawable instanceof CircleDrawable) {
            ((CircleDrawable) mBackgroundDrawable).setBorderWidth(0);
            ((CircleDrawable) mBackgroundDrawable).setCornerRadius(0);
        }

        invalidate();
    }
}

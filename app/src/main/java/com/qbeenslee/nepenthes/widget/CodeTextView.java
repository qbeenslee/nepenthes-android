package com.qbeenslee.nepenthes.widget;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * 自定义字体
 *
 * Created by qbeenslee on 2014/12/8.
 */

public class CodeTextView extends TextView {
    public CodeTextView(Context context) {
        super(context);
        style(context);
    }

    public CodeTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        style(context);
    }

    public CodeTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        style(context);
    }

    private void style(Context context) {
        Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/code-light.otf");
        setTypeface(typeface);
    }
}

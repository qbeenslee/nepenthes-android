package com.qbeenslee.nepenthes.ui;


import android.app.Activity;
import android.content.res.Configuration;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;

import com.qbeenslee.nepenthes.R;

import java.io.IOException;
import java.util.List;

/**
 * Camera
 * Created by qbeenslee on 2014/12/2.
 */
public class CameraActivity extends Activity implements SensorEventListener, SurfaceHolder.Callback, View.OnClickListener {
    public static final String TAG = "CameraFragment";
    private View view;
    private SurfaceView surfaceView = null;
    private SurfaceHolder surfaceHolder = null;
    // private float offsetAngle = 0f;
    private Button btnShot = null;
    private Camera camera = null;
    private List<Camera.Size> photoSizes = null, previewSizes;


    public CameraActivity() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);
        /*
        SensorManager sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        /sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ORIENTATION), SensorManager.SENSOR_DELAY_UI);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            offsetAngle = 0f;
        }*/
        initControls();
    }

    private void initControls() {
        btnShot = (Button) findViewById(R.id.button_camera_shoot);
        btnShot.setOnClickListener(this);
        surfaceView = (SurfaceView) findViewById(R.id.surface_camera);
        surfaceView.setOnClickListener(this);
        surfaceHolder = surfaceView.getHolder();
        //surfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        //surfaceHolder.setKeepScreenOn(true);
        surfaceHolder.setFixedSize(400, 300);
        surfaceHolder.addCallback(this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        camera.stopPreview();
        camera.release();
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            camera.setPreviewDisplay(surfaceHolder);
            camera.startPreview();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_camera_shoot: {
                shot();
            }
            break;
            case R.id.surface_camera: {
                camera.autoFocus(afcb);
            }
            break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        camera = Camera.open();
        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {
            camera.setDisplayOrientation(90);
        }
        Camera.Parameters parameters = camera.getParameters();
        parameters.setPictureFormat(ImageFormat.JPEG);
        parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        photoSizes = parameters.getSupportedPictureSizes();
        for (Camera.Size size : photoSizes) {
            if (size.width > 900 && size.width < 2000) {
                parameters.setPictureSize(size.width, size.height);
                break;
            }
        }
        previewSizes = parameters.getSupportedPreviewSizes();
        for (Camera.Size size : previewSizes) {
            if (size.width > 900 && size.width < 2000) {
                parameters.setPreviewSize(size.width, size.height);
                break;
            }
        }
        camera.setParameters(parameters);
    }


    private Camera.AutoFocusCallback afcb = new Camera.AutoFocusCallback() {

        @Override
        public void onAutoFocus(boolean success, Camera camera) {
            //camera.takePicture(null, null, jpeg);
        }
    };

    private void shot() {

    }

}
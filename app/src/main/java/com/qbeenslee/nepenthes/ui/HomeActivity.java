package com.qbeenslee.nepenthes.ui;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu.OnOpenListener;
import com.jeremyfeinstein.slidingmenu.lib.anim.CustomAnimation;
import com.qbeenslee.nepenthes.R;
import com.qbeenslee.nepenthes.adapter.NaviAdapter;
import com.qbeenslee.nepenthes.common.Message;
import com.qbeenslee.nepenthes.custom.ChooseDialog;
import com.qbeenslee.nepenthes.custom.CustomDialog;
import com.qbeenslee.nepenthes.custom.FixedViewPager;
import com.qbeenslee.nepenthes.fragment.LeftMenuFragment;
import com.qbeenslee.nepenthes.logic.BaseActivity;

import java.io.File;

public class HomeActivity extends BaseActivity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {
    private static Handler handler;
    private SlidingMenu leftmenu = null;
    private long lastExitTime = 0;
    private RadioGroup switcher = null;
    private Button btnCamera = null;
    private RadioButton naviPhotoWall, naviFavorite;
    private FixedViewPager viewPager;
    private FragmentManager fm;
    private int nowPage = 0;
    private int colorNormal, colorChecked;
    private static ChooseDialog chooseDialog = null;
    private ImageView imgviewMenu = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new HomeHandler();
        fm = getSupportFragmentManager();
        setContentView(R.layout.activity_home);
        initControls();
    }

    private void initControls() {
        colorNormal = getResources().getColor(R.color.black);
        colorChecked = getResources().getColor(R.color.hopegreen);
        viewPager = (FixedViewPager) findViewById(R.id.navi_view_pager);
        viewPager.setAdapter(new NaviAdapter(fm));
        switcher = (RadioGroup) findViewById(R.id.navi_switcher);
        switcher.setOnCheckedChangeListener(this);
        naviPhotoWall = (RadioButton) findViewById(R.id.navi_photowall);
        naviFavorite = (RadioButton) findViewById(R.id.navi_favorite);
        btnCamera = (Button) findViewById(R.id.button_camera);
        btnCamera.setOnClickListener(this);
        imgviewMenu = (ImageView) findViewById(R.id.topbar_menu);
        imgviewMenu.setOnClickListener(this);
        initLeftMenu();
    }

    @Override
    protected void onResume() {
        super.onResume();
        switcher.check(naviPhotoWall.getId());
        //viewPager.setCurrentItem(nowPage);
        //naviPhotoWall.setTextColor(colorChecked);
    }

    private void initLeftMenu() {
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        leftmenu = new SlidingMenu(this);
        leftmenu.attachToActivity(this, SlidingMenu.SLIDING_WINDOW);
        leftmenu.setMenu(R.layout.fragment_leftmenu_frame);
        leftmenu.setBackgroundColor(getResources().getColor(R.color.hopegreen));
        if (metrics.widthPixels > 0) {
            leftmenu.setBehindOffset((int) (metrics.widthPixels * 0.3));
        } else {
            leftmenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        }
        leftmenu.setBehindCanvasTransformer((new CustomAnimation()).getCustomZoomAnimation());
        leftmenu.setFadeDegree(0.32f);
        leftmenu.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        leftmenu.setMode(SlidingMenu.LEFT);
        leftmenu.setOnOpenListener(new OnOpenListener() {
            @Override
            public void onOpen() {
                imgviewMenu.setImageResource(android.R.drawable.ic_menu_delete);
            }
        });
        leftmenu.setOnClosedListener(new SlidingMenu.OnClosedListener() {

            @Override
            public void onClosed() {
                imgviewMenu.setImageResource(R.drawable.ic_menu);

            }
        });
        LeftMenuFragment lmf = new LeftMenuFragment();
        fm.beginTransaction().replace(R.id.leftmenu_frame, lmf, LeftMenuFragment.TAG).commit();
        //fm.beginTransaction().addToBackStack(LeftMenuFragment.TAG).commit();
    }


    public static void sendMessage(int what) {
        handler.obtainMessage(what).sendToTarget();
    }

    public static void sendMessage(int what, Object obj) {
        handler.obtainMessage(what, obj).sendToTarget();
    }

    public static void sendMessage(int what, int arg1, int arg2) {
        handler.obtainMessage(what, arg1, arg2).sendToTarget();
    }

    public static void sendMessage(int what, int arg1, int arg2, Object obj) {
        handler.obtainMessage(what, arg1, arg2, obj).sendToTarget();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_camera: {
                if (chooseDialog == null) {
                    chooseDialog = new ChooseDialog(HomeActivity.this);
                }
                chooseDialog.show();
            }
            break;
            case R.id.topbar_menu: {
                leftmenu.toggle();
            }
            break;
        }
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        naviPhotoWall.setTextColor(colorNormal);
        naviFavorite.setTextColor(colorNormal);
        switch (checkedId) {
            case R.id.navi_photowall: {
                nowPage = Message.NAVI.PHOTOWALL;
                naviPhotoWall.setTextColor(colorChecked);
            }
            break;
            case R.id.navi_favorite: {
                nowPage = Message.NAVI.FAVORITE;
                naviFavorite.setTextColor(colorChecked);
            }
            break;
            default:
                break;
        }
        if (viewPager.getCurrentItem() != nowPage) {
            viewPager.setCurrentItem(nowPage, false);
        }
    }

    public void onOpenedLister() {

    }

    class HomeHandler extends Handler {
        @Override
        public void dispatchMessage(android.os.Message msg) {
            switch (msg.what) {
                case Message.HANDLER.LEFTMENU_ABOUT: {
                    Intent intent = new Intent(HomeActivity.this, AboutActivity.class);
                    startActivity(intent);
                }
                break;
                case Message.HANDLER.LEFTMENU_EXIT: {
                    dialog2exit();
                }
                break;
                case Message.HANDLER.LEFTMENU_INFORMATION: {

                }
                break;
                case Message.HANDLER.LEFTMENU_SETTING: {
                    Intent settingIntent = new Intent(HomeActivity.this, SettingActivity.class);
                    startActivity(settingIntent);
                }
                break;
                case Message.HANDLER.LEFTMENU_USER: {
                }
                break;
                case Message.HANDLER.DIALOG_TAKEPHOTO: {
                    getPhoto();
                }
                break;
                case Message.HANDLER.DIALOG_CHOOSEGALLERY: {
                    Intent chooseIntent = new Intent(Intent.ACTION_GET_CONTENT);
                    chooseIntent.setType("image/*");
                    startActivityForResult(chooseIntent, Message.CODE.REQUEST_CHOOSEGALLERY);
                }
                break;
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Message.CODE.REQUEST_CHOOSEGALLERY: {
            }
            break;
        }
    }

    private void getPhoto() {
        File newFile = new File(Environment.getExternalStorageDirectory(), "test.jpg");
        Uri fileUri = Uri.fromFile(newFile);
        Intent intentCameraO = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intentCameraO.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        Intent intentCameraI = new Intent(HomeActivity.this, CameraActivity.class);
        startActivity(intentCameraI);
        /*
        if (IntentDetection.isAvailableIntent(HomeActivity.this, intentCameraO)) {
            startActivityForResult(intentCameraO, Message.CODE.REQUEST_TAKEPHOTO);
        } else {
            Intent intentCameraI = new Intent(HomeActivity.this, CameraActivity.class);
            startActivity(intentCameraI);
        }*/
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            if (leftmenu.isMenuShowing()) {
                leftmenu.showContent();
            } else {
                back2exit();
            }
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private void back2exit() {
        if ((System.currentTimeMillis() - lastExitTime) > 2000) {
            Toast.makeText(HomeActivity.this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
            lastExitTime = System.currentTimeMillis();
        } else {
            System.exit(0);
        }
    }

    private void dialog2exit() {
        CustomDialog.Builder builder = new CustomDialog.Builder(this);
        builder.setMessage(R.string.confrim_quit);
        builder.setTitle(R.string.quit);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                System.exit(0);
            }
        });
        builder.setNegativeButton(R.string.cancel, new android.content.DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }
}

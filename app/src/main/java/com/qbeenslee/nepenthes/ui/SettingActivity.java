package com.qbeenslee.nepenthes.ui;

import android.app.Activity;
import android.os.Bundle;

import com.qbeenslee.nepenthes.R;

/**
 * 设置
 * Created by qbeenslee on 2014/12/11.
 */

public class SettingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
    }
}

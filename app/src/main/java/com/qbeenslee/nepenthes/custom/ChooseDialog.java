package com.qbeenslee.nepenthes.custom;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.qbeenslee.nepenthes.R;
import com.qbeenslee.nepenthes.common.Message;
import com.qbeenslee.nepenthes.ui.HomeActivity;

/**
 * #
 * Created by qbeenslee on 2014/12/6.
 */

public class ChooseDialog extends Dialog implements View.OnClickListener {
    private Context context;
    private Button btnChooseGallery, btnTakePhoto, btnCancle;

    public ChooseDialog(Context context, int theme) {
        super(context, theme);
        this.context = context;
    }

    public ChooseDialog(Context context) {
        super(context, R.style.transparentFrameWindowStyle);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_ios);
        btnChooseGallery = (Button) findViewById(R.id.dialog_choose_gallery);
        btnChooseGallery.setOnClickListener(this);
        btnTakePhoto = (Button) findViewById(R.id.dialog_take_photo);
        btnTakePhoto.setOnClickListener(this);
        btnCancle = (Button) findViewById(R.id.dialog_choose_cancle);
        btnCancle.setOnClickListener(this);
        init();
    }

    private void init() {
        setCanceledOnTouchOutside(true);
        Window window = getWindow();
        window.setWindowAnimations(R.style.chsDlgAnimstyle);
        WindowManager.LayoutParams wl = window.getAttributes();
        wl.x = 0;
        wl.y = context.getResources().getDisplayMetrics().heightPixels;
        wl.width = ViewGroup.LayoutParams.MATCH_PARENT;
        wl.height = ViewGroup.LayoutParams.WRAP_CONTENT;
        onWindowAttributesChanged(wl);
    }

    @Override
    public void onClick(View v) {
        this.dismiss();
        switch (v.getId()) {
            case R.id.dialog_choose_gallery: {
                HomeActivity.sendMessage(Message.HANDLER.DIALOG_CHOOSEGALLERY);
            }
            break;
            case R.id.dialog_take_photo: {
                HomeActivity.sendMessage(Message.HANDLER.DIALOG_TAKEPHOTO);
            }
            break;
            case R.id.dialog_choose_cancle: {
            }
            break;
            default:
                break;
        }
    }
}
